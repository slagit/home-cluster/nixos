{
  description = "NixOS System Configuration";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-22.11";
    sops-nix.url = "github:Mic92/sops-nix";
    utils.url = "github:gytis-ivaskevicius/flake-utils-plus/v1.3.1";
  };
  outputs = inputs@{ self, nixpkgs, sops-nix, utils }: utils.lib.mkFlake {
    inherit self inputs;

    channels.nixpkgs.config.allowUnfree = true;

    hostDefaults = {
      modules = [
        sops-nix.nixosModules.sops
        ./modules/common.nix
        ./modules/idrac.nix
        ./modules/netboot.nix
        ./modules/users.nix
      ];
    };

    hosts = {
      compute0.modules = [
        ./hosts/compute0.nix
      ];
      compute1.modules = [
        ./hosts/compute1.nix
      ];
      compute2.modules = [
        ./hosts/compute2.nix
      ];
      controller0.modules = [
        ./hosts/controller0.nix
      ];
      idrac.modules = [
        ./hosts/idrac.nix
      ];
    };
  };
}
