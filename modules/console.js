import RFB from '/novnc/core/rfb.js';

let desktopName;

function connectedToServer(e) {
  status("Connected to " + desktopName);
}

function disconnectedFromServer(e) {
  if (e.detail.clean) {
    status("Disconnected");
  } else {
    status("Something went wrong, connection is closed");
  }
}

function updateDesktopName(e) {
  desktopName = e.detail.name;
}

function status(text) {
  document.getElementById('status').textContent = text;
}

export default function launch_console(host, path) {
  status("Connecting");
  const scheme = window.location.protocol === "https:" ? 'wss' : 'ws';
  const url = `${scheme}://${host}/${path}`;
  const rfb = new RFB(document.getElementById('screen'), url);
  rfb.addEventListener("connect",  connectedToServer);
  rfb.addEventListener("disconnect", disconnectedFromServer);
  rfb.addEventListener("desktopname", updateDesktopName);
  rfb.resizeSession = true;
  rfb.scaleViewport = true;
}
