{ config, lib, pkgs, ... }: let
  cfg = config.services.idrac;
  eachServer = f: (builtins.listToAttrs (lib.lists.imap0 f cfg.servers));
  launch = pkgs.writeScript "launch.sh" ''
    #!/bin/sh

    set -eu

    IDRAC_HOST=$1

    TEMP_DIR="$(mktemp -d)"
    function cleanup {
      rm -rf "$TEMP_DIR"
    }
    trap cleanup EXIT

    mkdir "$TEMP_DIR/lib"
    ${pkgs.curl}/bin/curl --fail --insecure --output "$TEMP_DIR/avctKVM.jar" https://$IDRAC_HOST/software/avctKVM.jar
    ${pkgs.curl}/bin/curl --fail --insecure --output "$TEMP_DIR/lib/avctKVMIOLinux64.jar" https://$IDRAC_HOST/software/avctKVMIOLinux64.jar
    ${pkgs.curl}/bin/curl --fail --insecure --output "$TEMP_DIR/lib/avctVMLinux64.jar" https://$IDRAC_HOST/software/avctVMLinux64.jar

    (cd "$TEMP_DIR/lib" && ${pkgs.openjdk8-bootstrap}/bin/jar -xf avctKVMIOLinux64.jar)
    (cd "$TEMP_DIR/lib" && ${pkgs.openjdk8-bootstrap}/bin/jar -xf avctVMLinux64.jar)
    #${pkgs.strace}/bin/strace -o $TEMP_DIR/log -f ${pkgs.openjdk8-bootstrap}/bin/java -cp "$TEMP_DIR/avctKVM.jar" -Djava.library.path="$TEMP_DIR/lib:${pkgs.gcc11}/lib" -Djava.security.properties=${security_properties} com.avocent.idrac.kvm.Main ip=$IDRAC_HOST kmport=5900 vport=5900 user=$IDRAC_USER passwd=$IDRAC_PASSWORD apcp=1 version=2 vmprivilege=true "helpurl=https://$IDRAC_HOST/help/contents.html"
    ${pkgs.openjdk8-bootstrap}/bin/java -cp "$TEMP_DIR/avctKVM.jar" -Djava.library.path="$TEMP_DIR/lib" -Djava.security.properties=${security_properties} com.avocent.idrac.kvm.Main ip=$IDRAC_HOST kmport=5900 vport=5900 user=$IDRAC_USER passwd=$IDRAC_PASSWORD apcp=1 version=2 vmprivilege=true "helpurl=https://$IDRAC_HOST/help/contents.html"
  '';
  security_properties = pkgs.writeText "java.security" ''
    jdk.tls.disabledAlgorithms=TLSv1, TLSv1.1, DES, MD5withRSA, \
        DH keySize < 1024, EC keySize < 224, 3DES_EDE_CBC, anon, NULL, \
        include jdk.disabled.namedCurves
  '';
  site = pkgs.symlinkJoin {
    name = "idrac-site";
    paths = [
      (pkgs.writeTextDir "index.html" ''
        <!DOCTYPE html>

        <html lang="en">
          <head>
            <meta charset="utf-8">
            <title>SLAGIT.net iDrac</title>
          </head>
          <body>
            <ul>
              ${lib.concatStrings (map (s: ''
                <li>
                  <strong>${s.name}</strong>
                  <a href="console/${s.name}">CONSOLE</a>
                  <a href="https://${s.name}.${cfg.domain}">iDRAC</a>
                </li>
              '') cfg.servers)}
            </ul>
          </body>
        </html>
      '')
      (pkgs.writeTextDir "console.css" (builtins.readFile ./console.css))
      (pkgs.writeTextDir "console.js" (builtins.readFile ./console.js))
    ] ++ (map (s: pkgs.writeTextDir "console/${s.name}/index.html" ''
      <!DOCTYPE html>

      <html lang="en">
        <head>
          <meta charset="utf-8">
          <title>${s.name} - SLAGIT.net iDrac</title>
          <link href="/console.css" rel="stylesheet">
        </head>
        <body>
          <div id="top_bar">
            <div id="status">Loading</div>
          </div>
          <div id="screen">
          </div>
          <script src="/console.js" type="module"></script>
          <script type="module" crossorigin="anonymous">
            import launch_console from '/console.js';
            launch_console('${cfg.domain}', 'websockify-${s.name}');
          </script>
        </body>
      </html>
    '') cfg.servers);
  };
in {
  options.services.idrac = {
    domain = lib.mkOption {
      description = "Parent domain hosting idrac site.";
      type = lib.types.str;
    };
    enable = lib.mkEnableOption "iDrac Proxy";
    interface = lib.mkOption {
      description = "Interface iDrac website is hosted on.";
      type = lib.types.str;
    };
    listenAddresses = lib.mkOption {
      description = "IP address(es) iDrac website is hosted on.";
      type = lib.types.listOf lib.types.str;
    };
    servers = lib.mkOption {
      description = "target iDrac configuration";
      type = lib.types.listOf (lib.types.submodule {
        options = {
          ip = lib.mkOption {
            description = lib.mdDoc "Remote IP for iDrac controller";
            type = lib.types.str;
          };
          name = lib.mkOption {
            description = lib.mdDoc "Short name of remote host";
            type = lib.types.str;
          };
        };
      });
    };
  };
  config = lib.mkIf cfg.enable {
    networking.firewall.interfaces."${cfg.interface}".allowedTCPPorts = [ 80 443 ];
    security.acme = {
      certs."${cfg.domain}" = {
        extraDomainNames = [ "*.${cfg.domain}" ];
        group = "nginx";
      };
    };
    services.nginx = {
      enable = true;
      virtualHosts = lib.trivial.mergeAttrs {
        "${cfg.domain}" = {
          inherit (cfg) listenAddresses;
          addSSL = true;
          locations = lib.trivial.mergeAttrs {
            "/novnc/".extraConfig = ''
              root ${pkgs.novnc}/share/webapps;
            '';
          } (eachServer (i: s: {
            name = "/websockify-${s.name}";
            value.extraConfig = ''
              proxy_pass http://127.0.0.1:${toString (8080 + i)}/;
              proxy_http_version 1.1;
              proxy_set_header Upgrade $http_upgrade;
              proxy_set_header Connection "Upgrade";
              proxy_set_header Host $host;
            '';
          }));
          root = site;
          sslCertificate = "/var/lib/acme/${cfg.domain}/cert.pem";
          sslCertificateKey = "/var/lib/acme/${cfg.domain}/key.pem";
        };
      } (eachServer (i: s: {
        name = "${s.name}.${cfg.domain}";
        value = {
          inherit (cfg) listenAddresses;
          addSSL = true;
          locations."/".extraConfig = ''
            proxy_pass https://${s.ip};
            proxy_set_header Host $host;
            proxy_ssl_verify off;
          '';
          sslCertificate = "/var/lib/acme/${cfg.domain}/cert.pem";
          sslCertificateKey = "/var/lib/acme/${cfg.domain}/key.pem";
        };
      }));
    };
    systemd.services = lib.foldr lib.trivial.mergeAttrs {} [
      (eachServer (i: s: {
        name = "websockify-${s.name}";
        value = {
          after = [ "xvnc-${s.name}.service" ];
          enable = true;
          serviceConfig = {
            ExecStart = "${pkgs.python311Packages.websockify}/bin/websockify 127.0.0.1:${toString (8080 + i)} --unix-target=/run/idrac-${s.name}/vnc.sock";
          };
          wantedBy = [ "multi-user.target" ];
        };
      }))
      (eachServer (i: s: {
        name = "xvnc-${s.name}";
        value = {
          enable = true;
          serviceConfig = {
            RuntimeDirectory = "idrac-${s.name}";
            ExecStart = "${pkgs.tigervnc}/bin/Xvnc -nolisten tcp -nolisten local -listen unix -rfbport=-1 -rfbunixpath=/run/idrac-${s.name}/vnc.sock -rfbunixmode=0600 -SecurityTypes=None :${toString i}";
            Group = "idrac";
            User = "idrac";
          };
          wantedBy = [ "multi-user.target" ];
        };
      }))
      (eachServer (i: s: {
        name = "openbox-${s.name}";
        value = {
          enable = true;
          environment.DISPLAY = ":${toString i}";
          serviceConfig = {
            ExecStart = "${pkgs.openbox}/bin/openbox";
            Group = "idrac";
            User = "idrac";
          };
          wantedBy = [ "multi-user.target" ];
        };
      }))
      (eachServer (i: s: {
        name = "avocent-${s.name}";
        value = {
          enable = true;
          environment.DISPLAY = ":${toString i}";
          serviceConfig = {
            EnvironmentFile = "/run/secrets/idrac-credentials";
            ExecStart = "${launch} ${s.ip}";
            Group = "idrac";
            User = "idrac";
          };
          wantedBy = [ "multi-user.target" ];
        };
      }))
    ];
    users.groups.idrac = {
    };
    users.users.idrac = {
      description = "iDrac UI";
      group = "idrac";
      isSystemUser = true;
    };
  };
}
