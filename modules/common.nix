{ ... }: {
  environment.etc."ssh/ca.pub".text = ''
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJsh+mxYZb9ceKGgWLPJLtO8MwlQDZbn7mNjQNq7vJ2q Demo CA
  '';

  networking.firewall.interfaces.ztly5yzqza.allowedTCPPorts = [ 22 ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  services = {
    openssh = {
      enable = true;
      extraConfig = ''
        TrustedUserCAKeys /etc/ssh/ca.pub
      '';
      openFirewall = false;
      passwordAuthentication = false;
      permitRootLogin = "no";
    };
    zerotierone = {
      enable = true;
      joinNetworks = [ "0cccb752f73b91d7" ];
    };
  };

  system.stateVersion = "22.11";
}
