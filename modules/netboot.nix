{ config, lib, pkgs, ... }: let
  cfg = config.services.netboot;
  netboot-efi = pkgs.fetchurl {
    hash = "sha256-/ze3kUf22oEmNeKASJA4ewBqhOd8K4pc2pW7j4IAXF0=";
    url = "https://boot.netboot.xyz/ipxe/netboot.xyz.efi";
  };
  netboot-kpxe = pkgs.fetchurl {
    hash = "sha256-1N80OG/BIwLFBktActX0j6+7dWbeDu3/5++Pwe78/yo=";
    url = "https://boot.netboot.xyz/ipxe/netboot.xyz.kpxe";
  };
in {
  options.services.netboot = {
    enable = lib.mkEnableOption "iDrac Proxy";
    interfaces = lib.mkOption {
      description = "The interfaces on which the DHCP server should listen.";
      type = lib.types.listOf lib.types.str;
    };
  };
  config = lib.mkIf cfg.enable {
    services = {
      atftpd = {
        enable = true;
        extraOptions = [ "--bind-address 10.10.4.2" ];
        root = pkgs.runCommand "tftpd_root" {} ''
          mkdir $out
          cp ${netboot-efi} $out/netboot.xyz.efi
          cp ${netboot-kpxe} $out/netboot.xyz.kpxe
        '';
      };
      dhcpd4 = {
        inherit (cfg) interfaces;
        enable = true;
        extraConfig = ''
          option domain-name "slagit.net";
          option domain-name-servers 1.1.1.1, 1.0.0.1;
          option arch code 93 = unsigned integer 16;

          subnet 10.10.4.0 netmask 255.255.255.0 {
            range 10.10.4.128 10.10.4.254;
            option routers 10.10.4.1;
            interface vlan4;
            if exists user-class and ( option user-class = "iPXE" ) {
              filename "http://boot.netboot.xyz/menu.ipxe";
            } elsif option arch = encode-int ( 16, 16 ) {
              filename "http://boot.netboot.xyz/ipxe/netboot.xyz.efi";
              option vendor-class-identifier "HTTPClient";
            } elsif option arch = 00:07 {
              filename "netboot.xyz.efi";
            } else {
              filename "netboot.xyz.kpxe";
            }
          }
        '';
      };
    };
  };
}
