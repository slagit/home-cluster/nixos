{ ... }: {
  imports = [
    ../modules/hardware/set_top.nix
  ];

  networking = {
    defaultGateway.address = "10.10.4.1";
    dhcpcd.enable = false;
    firewall.enable = false;
    hostName = "idrac";
    interfaces = {
      vlan3.ipv6.addresses = [{
        address = "fd5c:b521:20a4:4e31::2";
        prefixLength = 64;
      }];
      vlan4.ipv4.addresses = [{
        address = "10.10.4.2";
        prefixLength = 24;
      }];
    };
    nameservers = [ "2606:4700:4700::1111" "2001:4860:4860::8888" "1.1.1.1" "8.8.8.8" ];
    useDHCP = false;
    vlans.vlan3 = {
      id = 3;
      interface = "enp2s0";
    };
    vlans.vlan4 = {
      id = 4;
      interface = "enp2s0";
    };
  };

  security.acme = {
    acceptTerms = true;
    defaults = {
      credentialsFile = "/run/secrets/digitalocean-api-token";
      dnsProvider = "digitalocean";
      email = "kocha@slagit.net";
    };
  };

  services = {
    idrac = {
      domain = "idrac.slagit-cloud.net";
      enable = true;
      interface = "ztly5yzqza";
      listenAddresses = [ "[fd0c:ccb7:52f7:3b91:d799:9359:647b:8ee7]" ];
      servers = [
        { ip="[fd5c:b521:20a4:4e31::10]"; name="controller0"; }
        { ip="[fd5c:b521:20a4:4e31::20]"; name="compute0"; }
        { ip="[fd5c:b521:20a4:4e31::21]"; name="compute1"; }
        { ip="[fd5c:b521:20a4:4e31::22]"; name="compute2"; }
      ];
    };
    netboot = {
      enable = true;
      interfaces = [ "vlan4" ];
    };
  };

  sops = {
    defaultSopsFile = ./idrac.yaml;
    secrets = {
      digitalocean-api-token = {};
      idrac-credentials = {};
    };
  };
}
