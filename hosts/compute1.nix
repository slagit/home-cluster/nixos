{ ... }: {
  imports = [
    ../modules/hardware/r610.nix
  ];

  networking = {
    defaultGateway.address = "10.10.4.1";
    hostName = "compute1";
    interfaces.eno1 = {
      ipv4.addresses = [{
        address = "10.10.4.21";
        prefixLength = 24;
      }];
      ipv6.addresses = [{
        address = "fd5c:b521:20a4:6732::21";
        prefixLength = 64;
      }];
    };
    nameservers = [ "2606:4700:4700::1111" "2001:4860:4860::8888" "1.1.1.1" "8.8.8.8" ];
    useDHCP = false;
  };
}
